---
title: 'windows 下 Tor 代理 freenode IRC 聊天'
date: '2018-02-20'
categories:
  - Backup
tags:
  - Guide
---



### 前言

试过多个聊天软件，没一个让我觉得好用

* wechat/qq /国产必不可少之一 /需要 qq 号或手机号 /用户你好，我是你爸爸 /爱用用，不用滚蛋 /就视奸你咋地，你又不知道 /说一个关键字试试，分分钟查水表

* telegram /需要手机号

* signal /需要手机号

* tox /哇，这个是什么，我不知道哟

* matrix/riot /用的人太少

* xmpp/jabber /要加好友好麻烦的说

* etc.

唔，因为被前女友删号删的有阴影，所以十分不喜欢需要加好友的聊天软件

目前，一般也就用 iMessage/SMS 和亲人/朋友怼怼，e-mail 怼网络上的人聊，聊这个字有点不恰当，骚扰？哈

IRC 是我最喜欢的东西之一，喜欢程度不下 Usenet，可是就算加了遮罩也罩不住，无奈～ 只能借用 Tor 了

### 下载软件 /手头只有台 win7，so... /*nix 用户想必也不需要看我浪费口水

* [Tor Browser](http://torproject.org)

* [HexChat](https://hexchat.github.io)

* [Cygwin](https://cygwin.com)

下载软件安装好就不需要说了吧，没有 proxy/vpn 舍不得老人头吧

### 生成证书 /由于滥用，要 SASL EXTERNAL or ECDSA-NIST256P-CHALLENGE 方式之一登录，我只说 SASL EXTERNAL

打开 Cygwin

`openssl req -x509 -new -newkey rsa:4096 -sha256 -days 1000 -nodes -out client.pem -keyout client.pem`

生成证书，有一项不空就行

`openssl x509 -in client.pem -outform der | sha1sum -b | cut -d' ' -f1`

列出所生成证书指纹

把 `C:\cygwin64\home\Administrator` 中的 client.pem 复制到 `%appdata%\HexChat\certs` 中，certs 文件夹自己创建

### 在 freenode 创建帐户并添加指纹

`/nick username` /把用户名换成自己的

`/msg NickServ REGISTER password e-mail` /把密码和邮件地址换成自己的

邮箱收到验证邮件

复制其中的命令在 freenode 中完成验证

`/msg NickServ CERT ADD 指纹`

添加指纹

### 配置 HexChat

在 network list 中 add 一个 名字随意 /freenode-tor 这样

server 为 `freenodeok2gncmy.onion/6697`

然后下面六个框，第一个和后三个打勾

* Connect to selected server only
* Use SSL for all the servers on this network
* Accept invalid SSL certificates
* Use global user information

login method 选 `SASL EXTERNAL (cert)`

然后打开 settings/preference/network/network setup/proxy server 选择以下

```
Hostname: localhost
Port: 9150
Type: Socks5
User proxy for: All Connections
```

### 以上准备工作

打开 Tor browser 看下能否连上 Tor 网络，前置代理自备

然后退出，在 Desktop\Tor Browser\Browser\TorBrowser\Data\Tor 中的 torrc 文件里加两句

```
ExcludeNodes {cn},{hk},{mo},{sg},{th},{pk},{by},{ru},{ir},{vn},{ph},{my},{cu},{br},{kz},{kw},{lk},{ci},{tk},{tw},{kp},{sy}
ExcludeExitNodes {cn},{hk},{mo},{sg},{th},{pk},{by},{ru},{ir},{vn},{ph},{my},{cu},{br},{kz},{kw},{lk},{ci},{tk},{tw},{kp},{sy}
StrictNodes 1
```

再打开 Tor browser

然后重新打开 HexChat ，freenode-tor connect...

出现以下

```
* Looking up freenodeok2gncmy.onion
* Looking up localhost
* Connecting to localhost (::1:9150)
* * Certification info:
*   ....
```

如果真的按照我的步骤来的话应该会成功

```
* Connected. Now logging in.
* *** Looking up your hostname...
* *** Couldn't look up your hostname
* Capabilities supported: account-notify extended-join identify-msg multi-prefix sasl
* Capabilities requested: account-notify extended-join identify-msg multi-prefix sasl 
* Capabilities acknowledged: account-notify extended-join identify-msg multi-prefix sasl 
* Authenticating via SASL as username (EXTERNAL)
* You are now logged in as username.
* SASL authentication successful
```

然后

/whois username 看一下

```
* (~username@gateway/tor-sasl/username): realname
* zettel.freenode.net :Tor
* is using a secure connection
```

类似这样的说明成功哦，备份一下证书就行

### 参考链接：/感谢以下

* https://freenode.net/kb/answer/registration

* https://freenode.net/kb/answer/chat

* https://freenode.net/kb/answer/certfp

* https://github.com/freenode/web-7.0/issues/327

* https://baseclock.wordpress.com/2017/01/11/connecting-to-freenodes-tor-hidden-service-with-tor-sasl-external-and-ssl/












